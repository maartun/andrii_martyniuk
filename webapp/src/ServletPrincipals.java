
import DAO.PrincipalDAO;
import DAO.StudentDAO;
import DAO.TeacherDAO;
import models.Principal;
import models.Student;
import models.Teacher;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletPrincipals extends HttpServlet {
    private PrincipalDAO principalDAO;

    public void init() {
        principalDAO = new PrincipalDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        try {
            switch (action) {
                case "/new-principal-form":
                    showNewPrincipalForm(request,response);
                    break;
                case "/insert-principal":
                    insertPrincipal(request, response);
                    break;
                case "/delete-principal":
                    deletePrincipal(request, response);
                    break;
                case "/edit-principal":
                    showPrincipalEditForm(request, response);
                    break;
                case "/update-principal":
                    updatePrincipal(request, response);
                    break;
                case "/list-principals":
                    listPrincipal(request,response);
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }


    private void listPrincipal(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Principal> listPrincipal = principalDAO.selectAllPrincipals();
        request.setAttribute("listPrincipal", listPrincipal);
        RequestDispatcher dispatcher = request.getRequestDispatcher("principal-list.jsp");
        dispatcher.forward(request, response);
    }


    private void showNewPrincipalForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("principal-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showPrincipalEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Principal existingPrincipal = principalDAO.selectPrincipal(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("principal-form.jsp");
        request.setAttribute("principal", existingPrincipal);
        dispatcher.forward(request, response);
    }

    private void insertPrincipal(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");
        String higherEducat = request.getParameter("higherEducat");
        String exp = request.getParameter("exp");
        String work_period = request.getParameter("work_period");
        Principal newPrincipal  = new Principal(name, age,email, mobile, higherEducat, exp, work_period);
        principalDAO.setInsertPrincipalSql(newPrincipal);
        response.sendRedirect("list-principals");
    }

    private void updatePrincipal(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");
        String higherEducat = request.getParameter("higherEducat");
        String exp = request.getParameter("exp");
        String work_period = request.getParameter("work_period");
        Principal newPrincipal  = new Principal(id, name, age,email, mobile, higherEducat, exp, work_period);
        principalDAO.updatePrincipal(newPrincipal);
        response.sendRedirect("list-principals");
    }

    private void deletePrincipal(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        principalDAO.deletePrincipal(id);
        response.sendRedirect("list-principals");
    }

}
