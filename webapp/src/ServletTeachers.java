
import DAO.StudentDAO;
import DAO.TeacherDAO;
import models.Student;
import models.Teacher;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletTeachers extends HttpServlet {
    private TeacherDAO teacherDAO;

    public void init() {
        teacherDAO = new TeacherDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        try {
            switch (action) {
                case "/new-teacher-form":
                    showNewTeacherForm(request,response);
                    break;
                case "/insert-teacher":
                    insertTeacher(request, response);
                    break;
                case "/delete-teacher":
                    deleteTeacher(request, response);
                    break;
                case "/edit-teacher":
                    showTeacherEditForm(request, response);
                    break;
                case "/update-teacher":
                    updateTeacher(request, response);
                    break;
                case "/list-workers":
                    chooseWorkerList(request, response);
                    break;
                case  "/list-teachers":
                    listTeacher(request, response);
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void chooseWorkerList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("list-workers-choose.jsp");
        dispatcher.forward(request, response);
    }

    private void listTeacher(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Teacher> listTeacher = teacherDAO.selectAllTeachers();
        request.setAttribute("listTeacher", listTeacher);
        RequestDispatcher dispatcher = request.getRequestDispatcher("workers-list.jsp");
        dispatcher.forward(request, response);
    }


   private void showNewTeacherForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("teacher-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showTeacherEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Teacher existingTeacher = teacherDAO.selectTeacher(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("teacher-form.jsp");
        request.setAttribute("teacher", existingTeacher);
        dispatcher.forward(request, response);
    }

    private void insertTeacher(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name = request.getParameter("name");
        String subject = request.getParameter("subject");
        String age = request.getParameter("age");
        String higherEducat = request.getParameter("higherEducat");
        String exp = request.getParameter("exp");
        Teacher newTeacher  = new Teacher(name, subject, age, higherEducat, exp);
        teacherDAO.setInsertTeacherSql(newTeacher);
        response.sendRedirect("list-teachers");
    }

   private void updateTeacher(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String subject = request.getParameter("subject");
        String higherEduat = request.getParameter("higherEducat");
       String age = request.getParameter("age");
       String exp = request.getParameter("exp");

       Teacher book = new Teacher(id, name, subject, higherEduat, age, exp);
       teacherDAO.updateTeacher(book);
        response.sendRedirect("list-teachers");
    }

    private void deleteTeacher(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        teacherDAO.deleteTeacher(id);
        response.sendRedirect("list-teachers");
    }

}
