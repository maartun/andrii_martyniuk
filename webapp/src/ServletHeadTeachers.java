
import DAO.HeadTeacherDAO;
import DAO.StudentDAO;
import DAO.TeacherDAO;
import models.HeadTeacher;
import models.Student;
import models.Teacher;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletHeadTeachers extends HttpServlet {
    private HeadTeacherDAO headTeacherDAO;

    public void init() {
        headTeacherDAO = new HeadTeacherDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        try {
            switch (action) {
                case "/new-headteacher-form":
                    showNewHeadTeacherForm(request,response);
                    break;
                case "/insert-headteacher":
                    insertHeadTeacher(request, response);
                    break;
                case "/delete-headteacher":
                    deleteHeadTeacher(request, response);
                    break;
                case "/edit-headteacher":
                    showHeadTeacherEditForm(request, response);
                    break;
                case "/update-headteacher":
                    updateHeadTeacher(request, response);
                    break;
                case  "/list-headteachers":
                    listHeadTeacher(request, response);
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void listHeadTeacher(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<HeadTeacher> listHeadTeacher = headTeacherDAO.selectAllHeadTeachers();
        request.setAttribute("listHeadTeacher", listHeadTeacher);
        RequestDispatcher dispatcher = request.getRequestDispatcher("headteacher-list.jsp");
        dispatcher.forward(request, response);
    }


    private void showNewHeadTeacherForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("headteacher-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showHeadTeacherEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        HeadTeacher existingHeadTeacher = headTeacherDAO.selectHeadTeacher(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("headteacher-form.jsp");
        request.setAttribute("headTeacher", existingHeadTeacher);
        dispatcher.forward(request, response);
    }

    private void insertHeadTeacher(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name = request.getParameter("name");
        String subject = request.getParameter("subject");
        String age = request.getParameter("age");
        String higherEducat = request.getParameter("higherEducat");
        String exp = request.getParameter("exp");
        HeadTeacher newHeadTeacher  = new HeadTeacher(name, subject, age, higherEducat, exp);
        headTeacherDAO.setInsertHeadTeacherSql(newHeadTeacher);
        response.sendRedirect("list-headteachers");
    }

    private void updateHeadTeacher(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String subject = request.getParameter("subject");
        String higherEduat = request.getParameter("higherEducat");
        String age = request.getParameter("age");
        String exp = request.getParameter("exp");

        HeadTeacher headTeacher = new HeadTeacher(id, name, subject, higherEduat, age, exp);
        headTeacherDAO.updateHeadTeacher(headTeacher);
        response.sendRedirect("list-headteachers");
    }

    private void deleteHeadTeacher(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        headTeacherDAO.deleteHeadTeacher(id);
        response.sendRedirect("list-headteachers");
    }

}
