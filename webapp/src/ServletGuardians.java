
import DAO.GuardianDAO;
import DAO.HeadTeacherDAO;
import DAO.StudentDAO;
import DAO.TeacherDAO;
import models.Guardian;
import models.HeadTeacher;
import models.Student;
import models.Teacher;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletGuardians extends HttpServlet {
    private GuardianDAO guardianDAO;

    public void init() {
        guardianDAO = new GuardianDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        try {
            switch (action) {
                case "/new-guardian-form":
                    showNewGuardianForm(request,response);
                    break;
                case "/insert-guardian":
                    insertGuardian(request, response);
                    break;
                case "/delete-guardian":
                    deleteGuardian(request, response);
                    break;
                case "/edit-guardian":
                    showGuardianEditForm(request, response);
                    break;
                case "/update-guardian":
                    updateGuardian(request, response);
                    break;
                case  "/list-guardians":
                    listGuardian(request, response);
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void listGuardian(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Guardian> listGuardian = guardianDAO.selectAllGuardians();
        request.setAttribute("listGuardian", listGuardian);
        RequestDispatcher dispatcher = request.getRequestDispatcher("guardian-list.jsp");
        dispatcher.forward(request, response);
    }


    private void showNewGuardianForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("guardian-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showGuardianEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Guardian existingGuardian = guardianDAO.selectGuardian(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("guardian-form.jsp");
        request.setAttribute("guardian", existingGuardian);
        dispatcher.forward(request, response);
    }

    private void insertGuardian(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        int power = Integer.parseInt(request.getParameter("power"));
        String higherEducat = request.getParameter("higherEducat");
        String exp = request.getParameter("exp");
        Guardian newGuardian  = new Guardian(name, age, power, higherEducat, exp);
        guardianDAO.setInsertGuardianSql(newGuardian);
        response.sendRedirect("list-guardians");
    }

    private void updateGuardian(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        int power = Integer.parseInt(request.getParameter("power"));
        String higherEducat = request.getParameter("higherEducat");
        String exp = request.getParameter("exp");

        Guardian guardian = new Guardian(id, name, age, power, higherEducat,  exp);
        guardianDAO.updateGuardian(guardian);
        response.sendRedirect("list-guardians");
    }

    private void deleteGuardian(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        guardianDAO.deleteGuardian(id);
        response.sendRedirect("list-guardians");
    }

}
