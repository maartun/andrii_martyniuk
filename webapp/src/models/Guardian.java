package models;


public class Guardian extends Workers {
    protected String guardian_name;
    protected String guardian_age;
    protected int guardian_power;
    protected String higherEducat;
    protected String exp;
    protected int guardian_id;

    public Guardian(int guardian_id, String guardian_name, String guardian_age, int guardian_power, String higherEducat, String exp) {
        this.guardian_name = guardian_name;
        this.guardian_age = guardian_age;
        this.guardian_power = guardian_power;
        this.higherEducat = higherEducat;
        this.exp = exp;
        this.guardian_id = guardian_id;
    }

    public Guardian(String guardian_name, String guardian_age, int guardian_power, String higherEducat, String exp) {
        this.guardian_name = guardian_name;
        this.guardian_age = guardian_age;
        this.guardian_power = guardian_power;
        this.higherEducat = higherEducat;
        this.exp = exp;
    }

    public String getGuardian_name() {
        return guardian_name;
    }

    public void setGuardian_name(String guardian_name) {
        this.guardian_name = guardian_name;
    }

    public String getGuardian_age() {
        return guardian_age;
    }

    public void setGuardian_age(String guardian_age) {
        this.guardian_age = guardian_age;
    }

    public int getGuardian_power() {
        return guardian_power;
    }

    public void setGuardian_power(int guardian_power) {
        this.guardian_power = guardian_power;
    }

    public String getHigherEducat() {
        return higherEducat;
    }

    public void setHigherEducat(String higherEducat) {
        this.higherEducat = higherEducat;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public int getGuardian_id() {
        return guardian_id;
    }

    public void setGuardian_id(int guardian_id) {
        this.guardian_id = guardian_id;
    }

    @Override
    public String getSalary() {
        return "The salary for Guardian is 400 USD";
    }
}
