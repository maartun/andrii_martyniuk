package models;



public class Principal extends Workers {
    protected String principal_name;
    protected String principal_email;
    protected String principal_mobile;
    protected String higherEducat;
    protected String exp;
    protected int principal_id;
    protected String principal_age;
    protected String principal_work_period;

    

    public String getPrincipal_name() {
        return principal_name;
    }

    public void setPrincipal_name(String principal_name) {
        this.principal_name = principal_name;
    }

    public String getPrincipal_email() {
        return principal_email;
    }

    public void setPrincipal_email(String principal_email) {
        this.principal_email = principal_email;
    }

    public String getPrincipal_mobile() {
        return principal_mobile;
    }

    public void setPrincipal_mobile(String principal_mobile) {
        this.principal_mobile = principal_mobile;
    }

    public String getHigherEducat() {
        return higherEducat;
    }

    public void setHigherEducat(String higherEducat) {
        this.higherEducat = higherEducat;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public int getPrincipal_id() {
        return principal_id;
    }

    public void setPrincipal_id(int principal_id) {
        this.principal_id = principal_id;
    }

    public String getPrincipal_age() {
        return principal_age;
    }

    public void setPrincipal_age(String principal_age) {
        this.principal_age = principal_age;
    }

    public String getPrincipal_work_period() {
        return principal_work_period;
    }

    public void setPrincipal_work_period(String principal_work_period) {
        this.principal_work_period = principal_work_period;
    }

    public Principal(String principal_name, String principal_age, String principal_email, String principal_mobile, String higherEducat, String exp, String principal_work_period) {
        this.principal_name = principal_name;
        this.principal_email = principal_email;
        this.principal_mobile = principal_mobile;
        this.higherEducat = higherEducat;
        this.exp = exp;
        this.principal_age = principal_age;
        this.principal_work_period = principal_work_period;
    }

    public Principal(int principal_id, String principal_name, String principal_age, String principal_email, String principal_mobile, String higherEducat, String exp, String principal_work_period) {
        this.principal_name = principal_name;
        this.principal_email = principal_email;
        this.principal_mobile = principal_mobile;
        this.higherEducat = higherEducat;
        this.exp = exp;
        this.principal_age = principal_age;
        this.principal_work_period = principal_work_period;
        this.principal_id = principal_id;
    }
    public Principal(int principal_id) {
        this.principal_id = principal_id;
    }

    public String fireWorker() {
        return "Principal fires the worker";
    }

    @Override
    public String getSalary() {
        return "The salary for Principal is 950 USD";
    }

    @Override
    public String toString() {
        return "Principal " + super.toString() +
                ", principal_name = " + principal_name +
                ", principal_email = " + principal_email +
                ", principal_mobile = " + principal_mobile;
    }
}
