package models;



public class HeadTeacher extends Workers {
    protected String headTeacher_name;
    protected String headTeacher_subject;
    protected String headTeacher_age;
    protected String higherEducat;
    protected String exp;
    protected int headTeacher_id;

    public HeadTeacher(String headTeacher_name, String headTeacher_subject, String headTeacher_age, String higherEducat, String exp) {
        this.headTeacher_name = headTeacher_name;
        this.headTeacher_subject = headTeacher_subject;
        this.headTeacher_age = headTeacher_age;
        this.higherEducat = higherEducat;
        this.exp = exp;

    }

    public HeadTeacher(int headTeacher_id, String headTeacher_name, String headTeacher_subject, String headTeacher_age, String higherEducat, String exp) {
        this.headTeacher_name = headTeacher_name;
        this.headTeacher_subject = headTeacher_subject;
        this.headTeacher_age = headTeacher_age;
        this.higherEducat = higherEducat;
        this.exp = exp;
        this.headTeacher_id = headTeacher_id;
    }

    public String getHeadTeacher_name() {
        return headTeacher_name;
    }

    public void setHeadTeacher_name(String headTeacher_name) {
        this.headTeacher_name = headTeacher_name;
    }

    public String getHeadTeacher_subject() {
        return headTeacher_subject;
    }

    public void setHeadTeacher_subject(String headTeacher_subject) {
        this.headTeacher_subject = headTeacher_subject;
    }

    public String getHeadTeacher_age() {
        return headTeacher_age;
    }

    public void setHeadTeacher_age(String headTeacher_age) {
        this.headTeacher_age = headTeacher_age;
    }

    public String getHigherEducat() {
        return higherEducat;
    }

    public void setHigherEducat(String higherEducat) {
        this.higherEducat = higherEducat;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public int getHeadTeacher_id() {
        return headTeacher_id;
    }

    public void setHeadTeacher_id(int headTeacher_id) {
        this.headTeacher_id = headTeacher_id;
    }

    public String doEducatActivities() {
        return "Head Teacher is doing educational activities";
    }

    @Override
    public String getSalary() {
        return "The salary for Head Teacher is 750 USD";
    }

}