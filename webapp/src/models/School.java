package models;



public class School {
    protected int schoolNmb;
    protected String city;
    protected boolean isOpen;


    public School(int schoolNmb, String city) {
        this.schoolNmb = schoolNmb;
        this.city = city;
    }



    @Override
    public String toString() {
        return "School " +
                "schoolNmb=" + schoolNmb +
                ", city='" + city;
    }
}
