package models;



public class Teacher extends Workers {
    protected String teacher_name;
    protected String teacher_subject;
    protected String teacher_age;
    protected int teacher_id;
    protected String higherEducat;
    protected String exp;

    public String getHigherEducat() {
        return higherEducat;
    }

    public String getExp() {
        return exp;
    }

    public Teacher(String higherEducat, String exp, int teacher_id) {
        this.higherEducat = higherEducat;
        this.exp = exp;
        this.teacher_id = teacher_id;
    }

    public Teacher(int id, String teacher_name, String teacher_subject, String teacher_age, String higherEducat, String exp) {
        this.higherEducat = higherEducat;
        this.exp = exp;
        this.teacher_name = teacher_name;
        this.teacher_subject = teacher_subject;
        this.teacher_age = teacher_age;
        this.teacher_id = id;

    }

    public Teacher( String teacher_name, String teacher_subject, String teacher_age, String higherEducat, String exp) {
        this.higherEducat = higherEducat;
        this.exp = exp;
        this.teacher_name = teacher_name;
        this.teacher_subject = teacher_subject;
        this.teacher_age = teacher_age;


    }

    public String getTeacher_higherEducat() {
        return higherEducat;
    }

    public String getTeacher_exp() {
        return exp;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_subject() {
        return teacher_subject;
    }

    public void setTeacher_subject(String teacher_subject) {
        this.teacher_subject = teacher_subject;
    }

    public String getTeacher_age() {
        return teacher_age;
    }

    public void setTeacher_age(String teacher_age) {
        this.teacher_age = teacher_age;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public static String getHomeWork() {
        return "The teacher is giving home work...";
    }

    @Override
    public String getSalary() {
        return "The salary for Teacher is 500 USD";
    }

    @Override
    public String toString() {
        return "Teacher " + super.toString() +
                ", teacher_name = " + teacher_name +
                ", teacher_subject = " + teacher_subject +
                ", teacher_age = " + teacher_age;
    }
}
