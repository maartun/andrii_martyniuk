package DAO;

import models.Guardian;
import models.HeadTeacher;
import models.Student;
import models.Teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class GuardianDAO implements IGuardian {
    private String jdbcURL = "jdbc:mysql://localhost:3306/Students";
    private String jdbcUsername = "root";
    private String jdbcPassword = "1234";

    private static final String INSERT_GUARDIAN_SQL = "INSERT INTO guardians (name, age, power, higherEducat, exp) VALUES (?, ?, ?, ?, ?);";

    private static final String SELECT_GUARDIAN_BY_ID = "select id,name,age,power,higherEducat,exp from guardians where id =?";
    private static final String SELECT_ALL_GUARDIANS = "select * from guardians";
    private static final String DELETE_GUARDIAN_SQL = "delete from guardians where id = ?;";
    private static final String UPDATE_GUARDIAN_SQL = "update guardians set name = ?,age = ?, power = ?, higherEducat = ?, exp = ? where id = ?;";

    public GuardianDAO() {
    }

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public void setInsertGuardianSql(Guardian guardian) throws SQLException {
        System.out.println(INSERT_GUARDIAN_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_GUARDIAN_SQL)) {
            preparedStatement.setString(1, guardian.getGuardian_name());
            preparedStatement.setString(2, guardian.getGuardian_age());
            preparedStatement.setInt(3, guardian.getGuardian_power());
            preparedStatement.setString(4, guardian.getHigherEducat());
            preparedStatement.setString(5,guardian.getExp());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Guardian selectGuardian(int id) {
        Guardian guardian = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_GUARDIAN_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String name = rs.getString("name");
                String age = rs.getString("age");
                int power = rs.getInt("power");
                String higherEducat = rs.getString("higherEducat");
                String exp = rs.getString("exp");
                guardian = new Guardian(id, name, age, power, higherEducat,exp);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return guardian;
    }

    @Override
    public List<Guardian> selectAllGuardians() {
        List<Guardian> guardians = new ArrayList<>();
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_GUARDIANS);
            {
                System.out.println(preparedStatement);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    String age = rs.getString("age");
                    int power = rs.getInt("power");
                    String higherEducat = rs.getString("higherEducat");
                    String exp = rs.getString("exp");
                    guardians.add(new Guardian(id, name, age, power, higherEducat, exp));
                }
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return guardians;
    }

    @Override
    public boolean deleteGuardian(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_GUARDIAN_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    @Override
    public boolean updateGuardian(Guardian guardian) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_GUARDIAN_SQL);) {
            statement.setString(1, guardian.getGuardian_name());
            statement.setString(2, guardian.getGuardian_age());
            statement.setInt(3, guardian.getGuardian_power());
            statement.setString(4, guardian.getHigherEducat());
            statement.setString(5,guardian.getExp());
            statement.setInt(6, guardian.getGuardian_id());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

}

