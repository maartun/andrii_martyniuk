package DAO;

import models.HeadTeacher;
import models.Teacher;

import java.sql.SQLException;
import java.util.List;

public interface ITeacher {
    void setInsertTeacherSql(Teacher teacher) throws SQLException;
    Teacher selectTeacher(int id);
    List<Teacher> selectAllTeachers();
    boolean deleteTeacher(int id) throws SQLException;
    boolean updateTeacher(Teacher teacher) throws SQLException;
}
