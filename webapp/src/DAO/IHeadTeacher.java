package DAO;

import models.HeadTeacher;
import models.Principal;

import java.sql.SQLException;
import java.util.List;

public interface IHeadTeacher {
    void setInsertHeadTeacherSql(HeadTeacher headTeacher) throws SQLException;
    HeadTeacher selectHeadTeacher(int id);
    List<HeadTeacher> selectAllHeadTeachers();
    boolean deleteHeadTeacher(int id) throws SQLException;
    boolean updateHeadTeacher(HeadTeacher headTeacher) throws SQLException;
}
