package DAO;

import models.Student;
import models.Teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class TeacherDAO implements ITeacher {
    private String jdbcURL = "jdbc:mysql://localhost:3306/Students";
    private String jdbcUsername = "root";
    private String jdbcPassword = "1234";

    private static final String INSERT_TEACHER_SQL = "INSERT INTO Teachers (name, subject, age, higherEducat, exp) VALUES (?, ?, ?, ?, ?);";

    private static final String SELECT_TEACHER_BY_ID = "select id,name,subject,age,higherEducat,exp from Teachers where id =?";
    private static final String SELECT_ALL_TEACHERS = "select * from Teachers";
    private static final String DELETE_TEACHERS_SQL = "delete from Teachers where id = ?;";
    private static final String UPDATE_TEACHERS_SQL = "update Teachers set name = ?,subject = ?, age = ?, higherEducat = ?, exp = ? where id = ?;";

    public TeacherDAO() {
    }

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public void setInsertTeacherSql(Teacher teacher) throws SQLException {
        System.out.println(INSERT_TEACHER_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TEACHER_SQL)) {
            preparedStatement.setString(1, teacher.getTeacher_name());
            preparedStatement.setString(2, teacher.getTeacher_subject());
            preparedStatement.setString(3, teacher.getTeacher_age());
            preparedStatement.setString(4, teacher.getTeacher_higherEducat());
            preparedStatement.setString(5, teacher.getTeacher_exp());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Teacher selectTeacher(int id) {
        Teacher teacher = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TEACHER_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String name = rs.getString("name");
                String subject = rs.getString("subject");
                String age = rs.getString("age");
                String higherEducat = rs.getString("higherEducat");
                String exp = rs.getString("exp");
                teacher = new Teacher(id, name, subject, age, higherEducat,exp);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return teacher;
    }

    @Override
    public List<Teacher> selectAllTeachers() {
        List<Teacher> teachers = new ArrayList<>();
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_TEACHERS);
            {
                System.out.println(preparedStatement);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    String subject = rs.getString("subject");
                    String age = rs.getString("age");
                    String higherEducat = rs.getString("higherEducat");
                    String exp = rs.getString("exp");
                    teachers.add(new Teacher(id, name, subject, age, higherEducat, exp));
                }
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return teachers;
    }

    @Override
    public boolean deleteTeacher(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_TEACHERS_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    @Override
    public boolean updateTeacher(Teacher teacher) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_TEACHERS_SQL);) {
            statement.setString(1, teacher.getTeacher_name());
            statement.setString(2, teacher.getTeacher_subject());
            statement.setString(3, teacher.getTeacher_age());
            statement.setString(4, teacher.getTeacher_higherEducat());
            statement.setString(5, teacher.getTeacher_exp());
            statement.setInt(6, teacher.getTeacher_id());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

}

