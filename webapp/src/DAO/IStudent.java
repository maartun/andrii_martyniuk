package DAO;

import models.Student;

import java.sql.SQLException;
import java.util.List;

public interface IStudent {
    void insertStudent(Student student) throws SQLException;
    Student selectStudent(int id);
    List<Student> selectAllStudents();
    boolean deleteStudent(int id) throws SQLException;
    boolean updateStudent(Student student) throws SQLException;
}
