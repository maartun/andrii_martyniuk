package DAO;

import models.Guardian;
import models.Principal;

import java.sql.SQLException;
import java.util.List;

public interface IGuardian {
    void setInsertGuardianSql(Guardian guardian) throws SQLException;
    Guardian selectGuardian(int id);
    List<Guardian> selectAllGuardians();
    boolean deleteGuardian(int id) throws SQLException;
    boolean updateGuardian(Guardian guardian) throws SQLException;
}
