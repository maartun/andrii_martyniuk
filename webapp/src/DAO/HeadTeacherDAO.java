package DAO;

import models.HeadTeacher;
import models.Student;
import models.Teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class HeadTeacherDAO implements IHeadTeacher {
    private String jdbcURL = "jdbc:mysql://localhost:3306/Students";
    private String jdbcUsername = "root";
    private String jdbcPassword = "1234";

    private static final String INSERT_HEADTEACHER_SQL = "INSERT INTO headteachers (name, subject, age, higherEducat, exp) VALUES (?, ?, ?, ?, ?);";

    private static final String SELECT_HEADTEACHER_BY_ID = "select id,name,subject,age,higherEducat,exp from headteachers where id =?";
    private static final String SELECT_ALL_HEADTEACHERS = "select * from headteachers";
    private static final String DELETE_HEADTEACHER_SQL = "delete from headteachers where id = ?;";
    private static final String UPDATE_HEADTEACHER_SQL = "update headteachers set name = ?,subject = ?, age = ?, higherEducat = ?, exp = ? where id = ?;";

    public HeadTeacherDAO() {
    }

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public void setInsertHeadTeacherSql(HeadTeacher headTeacher) throws SQLException {
        System.out.println(INSERT_HEADTEACHER_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HEADTEACHER_SQL)) {
            preparedStatement.setString(1, headTeacher.getHeadTeacher_name());
            preparedStatement.setString(2, headTeacher.getHeadTeacher_subject());
            preparedStatement.setString(3, headTeacher.getHeadTeacher_age());
            preparedStatement.setString(4, headTeacher.getHigherEducat());
            preparedStatement.setString(5,headTeacher.getExp());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public HeadTeacher selectHeadTeacher(int id) {
        HeadTeacher headteacher = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_HEADTEACHER_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String name = rs.getString("name");
                String subject = rs.getString("subject");
                String age = rs.getString("age");
                String higherEducat = rs.getString("higherEducat");
                String exp = rs.getString("exp");
                headteacher = new HeadTeacher(id, name, subject, age, higherEducat,exp);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return headteacher;
    }

    @Override
    public List<HeadTeacher> selectAllHeadTeachers() {
        List<HeadTeacher> headTeachers = new ArrayList<>();
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_HEADTEACHERS);
            {
                System.out.println(preparedStatement);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    String subject = rs.getString("subject");
                    String age = rs.getString("age");
                    String higherEducat = rs.getString("higherEducat");
                    String exp = rs.getString("exp");
                    headTeachers.add(new HeadTeacher(id, name, subject, age, higherEducat, exp));
                }
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return headTeachers;
    }

    @Override
    public boolean deleteHeadTeacher(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_HEADTEACHER_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    @Override
    public boolean updateHeadTeacher(HeadTeacher headTeacher) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_HEADTEACHER_SQL);) {
            statement.setString(1, headTeacher.getHeadTeacher_name());
            statement.setString(2, headTeacher.getHeadTeacher_subject());
            statement.setString(3, headTeacher.getHeadTeacher_age());
            statement.setString(4, headTeacher.getHigherEducat());
            statement.setString(5, headTeacher.getExp());
            statement.setInt(6, headTeacher.getHeadTeacher_id());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

}

