package DAO;

import models.Principal;
import models.Student;

import java.sql.SQLException;
import java.util.List;

public interface IPrincipal {
    void setInsertPrincipalSql(Principal principal) throws SQLException;
    Principal selectPrincipal(int id);
    List<Principal> selectAllPrincipals();
    boolean deletePrincipal(int id) throws SQLException;
    boolean updatePrincipal(Principal principal) throws SQLException;
}
