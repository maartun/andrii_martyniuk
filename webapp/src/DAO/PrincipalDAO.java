package DAO;

import models.Principal;
import models.Student;
import models.Teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class PrincipalDAO implements IPrincipal {
    private String jdbcURL = "jdbc:mysql://localhost:3306/Students";
    private String jdbcUsername = "root";
    private String jdbcPassword = "1234";

    private static final String INSERT_PRINCIPAL_SQL = "INSERT INTO Principals (name, age, email, mobile, higherEducat, exp, work_period) VALUES (?, ?, ?, ?, ?, ?, ?);";

    private static final String SELECT_PRINCIPAL_BY_ID = "select id,name, age, email, mobile, higherEducat, exp, work_period from Principals where id =?";
    private static final String SELECT_ALL_PRINCIPALS = "select * from Principals";
    private static final String DELETE_PRINCIPAL_SQL = "delete from Principals where id = ?;";
    private static final String UPDATE_PRINCIPAL_SQL = "update Principals set name = ?, age = ?, email = ?, mobile = ?, higherEducat = ?, exp = ?, work_period = ? where id = ?;";

    public PrincipalDAO() {
    }

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }

    public void setInsertPrincipalSql(Principal principal) throws SQLException {
        System.out.println(INSERT_PRINCIPAL_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PRINCIPAL_SQL)) {
            preparedStatement.setString(1, principal.getPrincipal_name());
            preparedStatement.setString(2, principal.getPrincipal_age());
            preparedStatement.setString(3, principal.getPrincipal_email());
            preparedStatement.setString(4, principal.getPrincipal_mobile());
            preparedStatement.setString(5, principal.getHigherEducat());
            preparedStatement.setString(6, principal.getExp());
            preparedStatement.setString(7, principal.getPrincipal_work_period());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Principal selectPrincipal(int id) {
        Principal principal = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PRINCIPAL_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String name = rs.getString("name");
                String age = rs.getString("age");
                String email = rs.getString("email");
                String mobile = rs.getString("mobile");
                String higherEducat = rs.getString("higherEducat");
                String exp = rs.getString("exp");
                String workPeriod = rs.getString("work_period");
                principal = new Principal(id, name, age, email, mobile, higherEducat,exp, workPeriod);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return principal;
    }

    @Override
    public List<Principal> selectAllPrincipals() {
        List<Principal> principals = new ArrayList<>();
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_PRINCIPALS);
            {
                System.out.println(preparedStatement);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    String age = rs.getString("age");
                    String email = rs.getString("email");
                    String mobile = rs.getString("mobile");
                    String higherEducat = rs.getString("higherEducat");
                    String exp = rs.getString("exp");
                    String workPeriod = rs.getString("work_period");
                    principals.add(new Principal(id, name, age, email, mobile, higherEducat, exp, workPeriod));
                }
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return principals;
    }

    @Override
    public boolean deletePrincipal(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_PRINCIPAL_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    @Override
    public boolean updatePrincipal(Principal principal) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_PRINCIPAL_SQL);) {
            statement.setString(1, principal.getPrincipal_name());
            statement.setString(2, principal.getPrincipal_age());
            statement.setString(3, principal.getPrincipal_email());
            statement.setString(4, principal.getPrincipal_mobile());
            statement.setString(5, principal.getHigherEducat());
            statement.setString(6, principal.getExp());
            statement.setString(7, principal.getPrincipal_work_period());
            statement.setInt(8, principal.getPrincipal_id());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

}

