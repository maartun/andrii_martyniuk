<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>School Management Application</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="background-color: darkblue">
        <div>
            <a href="<%=request.getContextPath()%>/list" class="navbar-brand"> School Management App </a>
        </div>
    </nav>
</header>
<br>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <c:if test="${teacher != null}">
            <form action="update-teacher" method="post">
                </c:if>
                <c:if test="${teacher == null}">
                <form action="insert-teacher" method="post">
                    </c:if>

                    <caption>
                        <h2>
                            <c:if test="${teacher != null}">
                                Edit Teacher
                            </c:if>
                            <c:if test="${teacher == null}">
                                Add New Teacher
                            </c:if>
                        </h2>
                    </caption>

                    <c:if test="${teacher != null}">
                        <input type="hidden" name="id" value="<c:out value='${teacher.teacher_id}' />" />
                    </c:if>

                    <fieldset class="form-group">
                        <label>Teacher Name</label> <input type="text" value="<c:out value='${teacher.teacher_name}' />" class="form-control" name="name" required="required">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Teacher subject</label> <input type="text" value="<c:out value='${teacher.teacher_subject}' />" class="form-control" name="subject">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Teacher age</label> <input type="text" value="<c:out value='${teacher.teacher_age}' />" class="form-control" name="age">
                    </fieldset>
                        <fieldset class="form-group">
                            <label>Teacher Higher Education</label> <input type="text" value="<c:out value='${teacher.higherEducat}' />" class="form-control" name="higherEducat">
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Teacher experience</label> <input type="text" value="<c:out value='${teacher.exp}' />" class="form-control" name="exp">
                        </fieldset>
                    <button type="submit" class="btn btn-success">Save</button>
                </form>
        </div>
    </div>
</div>
</body>

</html>