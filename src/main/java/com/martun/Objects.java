package com.martun;

public enum Objects {
    GUARDIAN,
    HEADTEACHER,
    PRINCIPAL,
    STUDENT,
    TEACHER;
}
