package com.martun;

import com.martun.models.Student;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class StudentsDataBase {
    public static List<Student> studentsDataBase() throws ClassNotFoundException, SQLException {
        List<Student> rst;
        String userName = "root";
        String password = "1234";
        String connectionUrl = "jdbc:mysql://localhost:3306/Students";
        Class.forName("com.mysql.cj.jdbc.Driver");
        try (Connection connection = DriverManager.getConnection(connectionUrl, userName, password);
             Statement statement = connection.createStatement()) {
            statement.executeUpdate("drop table students");
            statement.executeUpdate("CREATE TABLE Students(id int not null auto_increment, name varchar (255), " +
                    "class varchar (255), birthdate varchar(255) , primary key (id))");
            ResultSet resultSet = statement.executeQuery("select * from students");

            statement.executeUpdate("CREATE TABLE Teachers(id int not null auto_increment, name varchar (255), " +
                    "subject varchar (255), age varchar(255) higherEducat varchar (255), exp int, primary key (id))");
            rst = new LinkedList<>();

            while (resultSet.next()) {
                String i = resultSet.getString("name");
                String str = resultSet.getString("class");
                String str1 = resultSet.getString("birthdate");
                Student stdnt = new Student(i, str, str1);
                rst.add(stdnt);
            }
        }
        return rst;
    }
}