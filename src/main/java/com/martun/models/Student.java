package com.martun.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

import static com.martun.StudentsDataBase.studentsDataBase;

public class Student {
    protected int id;
    protected String student_name;
    protected String student_class;
    protected String student_birthDate;
    public static final Logger logger = LoggerFactory.getLogger(Student.class);

    public Student(List<Student> studentsDataBase) throws SQLException, ClassNotFoundException {
        studentsDataBase = studentsDataBase();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public void setStudent_class(String student_class) {
        this.student_class = student_class;
    }

    public void setStudent_birthDate(String student_birthDate) {
        this.student_birthDate = student_birthDate;
    }

    public String getStudent_name() {
        return student_name;
    }

    public String getStudent_class() {
        return student_class;
    }

    public String getStudent_birthDate() {
        return student_birthDate;
    }

    public String getStudentName() {
        return student_name;
    }

    public String getStudentClass() {
        return student_class;
    }

    public String getStudentBirthDate() {
        return student_birthDate;
    }

    public int getId() {
        return id;
    }
    public Student(){

    }
    public Student(int id){
        this.id = id;
    }
    public Student(String student_name, String student_class, String student_birthDate) {
        this.student_name = student_name;
        this.student_class = student_class;
        this.student_birthDate = student_birthDate;
    }

    public Student(int id, String student_name, String student_class, String student_birthDate) {
        this.id = id;
        this.student_name = student_name;
        this.student_class = student_class;
        this.student_birthDate = student_birthDate;
    }

    public String doHomeWork() {
        return "The student is doing home work...";
    }

    @Override
    public String toString() {
        return "Student " +
                ", student_name = " + student_name +
                ", student_class = " + student_class +
                ", student_birthDate = " + student_birthDate;
    }
}
