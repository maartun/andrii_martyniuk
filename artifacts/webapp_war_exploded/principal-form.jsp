<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>School Management Application</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="background-color: darkblue">
        <div>
            <a href="<%=request.getContextPath()%>/list" class="navbar-brand"> School Management App </a>
        </div>
    </nav>
</header>
<br>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <c:if test="${principal != null}">
            <form action="update-principal" method="post">
                </c:if>
                <c:if test="${principal == null}">
                <form action="insert-principal" method="post">
                    </c:if>

                    <caption>
                        <h2>
                            <c:if test="${principal != null}">
                                Edit Principal
                            </c:if>
                            <c:if test="${principal == null}">
                                Add New Principal
                            </c:if>
                        </h2>
                    </caption>

                    <c:if test="${principal != null}">
                        <input type="hidden" name="id" value="<c:out value='${principal.principal_id}' />" />
                    </c:if>

                    <fieldset class="form-group">
                        <label>Principal Name</label> <input type="text" value="<c:out value='${principal.principal_name}' />" class="form-control" name="name" required="required">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Principal age</label> <input type="text" value="<c:out value='${principal.principal_age}' />" class="form-control" name="age">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Principal email</label> <input type="text" value="<c:out value='${principal.principal_email}' />" class="form-control" name="email">
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Principal mobile</label> <input type="text" value="<c:out value='${principal.principal_mobile}' />" class="form-control" name="mobile">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Principal Higher Education</label> <input type="text" value="<c:out value='${principal.higherEducat}' />" class="form-control" name="higherEducat">
                    </fieldset>

                        <fieldset class="form-group">
                            <label>Principal experience</label> <input type="text" value="<c:out value='${principal.exp}' />" class="form-control" name="exp">
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Principal work period</label> <input type="text" value="<c:out value='${principal.principal_work_period}' />" class="form-control" name="work_period">
                        </fieldset>
                    <button type="submit" class="btn btn-success">Save</button>
                </form>
        </div>
    </div>
</div>
</body>

</html>