<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>School Management Application</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="background-color: darkblue">
        <div>
            <a href="<%=request.getContextPath()%>/list" class="navbar-brand"> School Management App </a>
        </div>
    </nav>
</header>
<br>

                                                <!-- TEACHERS -->
<div class="row">
    <div class="container">
        <h3 class="text-center">List of Teachers</h3>
        <hr>
        <div class="container text-left">
            <a href="<%=request.getContextPath()%>/new-teacher-form" class="btn btn-info">Add New Teacher</a>
        </div>
        <br>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Subject</th>
                <th>Age</th>
                <th>Higher Educate</th>
                <th>Experience</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="teacher" items="${listTeacher}">
                <tr>
                    <td>
                        <c:out value="${teacher.teacher_id}" />
                    </td>
                    <td>
                        <c:out value="${teacher.teacher_name}" />
                    </td>
                    <td>
                        <c:out value="${teacher.teacher_subject}" />
                    </td>
                    <td>
                        <c:out value="${teacher.teacher_age}" />
                    </td>
                    <td>
                        <c:out value="${teacher.higherEducat}" />
                    </td>
                    <td>
                        <c:out value="${teacher.exp}" />
                    </td>
                    <td><a href="edit-teacher?id=<c:out value='${teacher.teacher_id}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp; <a href="delete-teacher?id=<c:out value='${teacher.teacher_id}' />">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>