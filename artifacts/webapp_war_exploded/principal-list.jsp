<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>School Management Application</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="background-color: darkblue">
        <div>
            <a href="<%=request.getContextPath()%>/list" class="navbar-brand"> School Management App </a>
        </div>
    </nav>
</header>
<br>

<!-- PRINCIPAL -->
<div class="row">
    <div class="container">
        <h3 class="text-center">List of Principal</h3>
        <hr>
        <div class="container text-left">
            <a href="<%=request.getContextPath()%>/new-principal-form" class="btn btn-info">Add New Principal</a>
        </div>

        <br>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Higher Educate</th>
                <th>Experience</th>
                <th>Work Period</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="principal" items="${listPrincipal}">
                <tr>
                    <td>
                        <c:out value="${principal.principal_id}" />
                    </td>
                    <td>
                        <c:out value="${principal.principal_name}" />
                    </td>
                    <td>
                        <c:out value="${principal.principal_age}" />
                    </td>
                    <td>
                        <c:out value="${principal.principal_email}" />
                    </td>
                    <td>
                        <c:out value="${principal.principal_mobile}" />
                    </td>
                    <td>
                        <c:out value="${principal.higherEducat}" />
                    </td>
                    <td>
                        <c:out value="${principal.exp}" />
                    </td>
                    <td>
                        <c:out value="${principal.principal_work_period}" />
                    </td>
                    <td><a href="edit-principal?id=<c:out value='${principal.principal_id}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp; <a href="delete-principal?id=<c:out value='${principal.principal_id}' />">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>