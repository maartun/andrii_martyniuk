<%--
  Created by IntelliJ IDEA.
  User: andru
  Date: 5/15/2020
  Time: 9:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>School Management Application</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="background-color: darkblue">
        <div>
            <a href="<%=request.getContextPath()%>/list" class="navbar-brand"> School Management App </a>
        </div>
    </nav>
</header>
<br>

<!-- TEACHERS -->
<div class="row">
    <div class="container">
        <h3 class="text-center">List of Guardians</h3>
        <hr>
        <div class="container text-left">
            <a href="<%=request.getContextPath()%>/new-guardian-form" class="btn btn-info">Add New Guardian</a>
        </div>
        <br>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
                <th>Power</th>
                <th>Higher Education</th>
                <th>Experience</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="guardian" items="${listGuardian}">
                <tr>
                    <td>
                        <c:out value="${guardian.guardian_id}" />
                    </td>
                    <td>
                        <c:out value="${guardian.guardian_name}" />
                    </td>
                    <td>
                        <c:out value="${guardian.guardian_age}" />
                    </td>
                    <td>
                        <c:out value="${guardian.guardian_power}" />
                    </td>
                    <td>
                        <c:out value="${guardian.higherEducat}" />
                    </td>
                    <td>
                        <c:out value="${guardian.exp}" />
                    </td>
                    <td><a href="edit-guardian?id=<c:out value='${guardian.guardian_id}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp; <a href="delete-guardian?id=<c:out value='${guardian.guardian_id}' />">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>