<%--
  Created by IntelliJ IDEA.
  User: andru
  Date: 5/15/2020
  Time: 9:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>School Management Application</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="background-color: darkblue">
        <div>
            <a href="<%=request.getContextPath()%>/list" class="navbar-brand"> School Management App </a>
        </div>
    </nav>
</header>
<br>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <c:if test="${guardian != null}">
            <form action="update-guardian" method="post">
                </c:if>
                <c:if test="${guardian == null}">
                <form action="insert-guardian" method="post">
                    </c:if>

                    <caption>
                        <h2>
                            <c:if test="${guardian != null}">
                                Edit Guardian
                            </c:if>
                            <c:if test="${guardian == null}">
                                Add New Guardian
                            </c:if>
                        </h2>
                    </caption>

                    <c:if test="${guardian != null}">
                        <input type="hidden" name="id" value="<c:out value='${guardian.guardian_id}' />" />
                    </c:if>

                    <fieldset class="form-group">
                        <label>Guardian Name</label> <input type="text" value="<c:out value='${guardian.guardian_name}' />" class="form-control" name="name" required="required">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Guardian age</label> <input type="text" value="<c:out value='${guardian.guardian_age}' />" class="form-control" name="age">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Guardian power</label> <input type="text" value="<c:out value='${guardian.guardian_power}' />" class="form-control" name="power">
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Guardian Higher Education</label> <input type="text" value="<c:out value='${guardian.higherEducat}' />" class="form-control" name="higherEducat">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Guardian experience</label> <input type="text" value="<c:out value='${guardian.exp}' />" class="form-control" name="exp">
                    </fieldset>
                    <button type="submit" class="btn btn-success">Save</button>
                </form>
        </div>
    </div>
</div>
</body>

</html>