<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>School Management Application</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark" style="background-color: darkblue">
        <div>
            <a href="<%=request.getContextPath()%>/list" class="navbar-brand"> School Management App </a>
        </div>
    </nav>
</header>
<br>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <c:if test="${headTeacher != null}">
            <form action="update-headteacher" method="post">
                </c:if>
                <c:if test="${headTeacher == null}">
                <form action="insert-headteacher" method="post">
                    </c:if>

                    <caption>
                        <h2>
                            <c:if test="${headTeacher != null}">
                                Edit Head Teacher
                            </c:if>
                            <c:if test="${headTeacher == null}">
                                Add New Head Teacher
                            </c:if>
                        </h2>
                    </caption>

                    <c:if test="${headTeacher != null}">
                        <input type="hidden" name="id" value="<c:out value='${headTeacher.headTeacher_id}' />" />
                    </c:if>

                    <fieldset class="form-group">
                        <label>Head Teacher Name</label> <input type="text" value="<c:out value='${headTeacher.headTeacher_name}' />" class="form-control" name="name" required="required">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Head Teacher subject</label> <input type="text" value="<c:out value='${headTeacher.headTeacher_subject}' />" class="form-control" name="subject">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Head Teacher age</label> <input type="text" value="<c:out value='${headTeacher.headTeacher_age}' />" class="form-control" name="age">
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Head Teacher Higher Education</label> <input type="text" value="<c:out value='${headTeacher.higherEducat}' />" class="form-control" name="higherEducat">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Head Teacher experience</label> <input type="text" value="<c:out value='${headTeacher.exp}' />" class="form-control" name="exp">
                    </fieldset>
                    <button type="submit" class="btn btn-success">Save</button>
                </form>
        </div>
    </div>
</div>
</body>

</html>